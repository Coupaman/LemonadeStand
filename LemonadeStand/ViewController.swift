//
//  ViewController.swift
//  LemonadeStand
//
//  Created by Neil on 06/11/2014.
//  Copyright (c) 2014 Coupaman. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    @IBOutlet weak var labelCash: UILabel!
    @IBOutlet weak var labelLemons: UILabel!
    @IBOutlet weak var labelIce: UILabel!

    @IBOutlet weak var labelLemonMix: UILabel!
    @IBOutlet weak var labelIceMix: UILabel!
    
    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var labelCustomers: UILabel!
    @IBOutlet weak var labelRevenue: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!

    @IBOutlet weak var sellLemonadeButton: UIButton!
    
//    let kInitialCash = 10
//    let kInitialLemons = 1
//    let kInitialIce = 1
//    let kLemonCost = 2
//    let kIceCost = 1
    
    var totalCash:Int = 10
    var lemonStock = 1
    var iceStock = 1
    var lemonMix = 0
    var iceMix = 0
    var dayCount = 0
    var weatherForecast = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        sellLemonadeButton.enabled = false
        sellLemonadeButton.layer.borderWidth = 1.0
        sellLemonadeButton.layer.cornerRadius = 10
        
        resetGame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func sellLemon(sender: UIButton) {
        
        if lemonStock > 0 {
            lemonStock--
            totalCash += 2
            labelLemons.text = "\(lemonStock)"
            labelCash.text = "£\(totalCash)"
        }
    }
    
    @IBAction func buyLemon(sender: UIButton) {
        
        if totalCash >= 2 {
            lemonStock++
            totalCash -= 2
            labelLemons.text = "\(lemonStock)"
            labelCash.text = "£\(totalCash)"
        }
    }
    
    @IBAction func sellIce(sender: UIButton) {
        if iceStock > 0 {
            iceStock--
            totalCash += 1
            labelIce.text = "\(iceStock)"
            labelCash.text = "£\(totalCash)"
        }
    }
    
    @IBAction func buyIce(sender: UIButton) {

        if totalCash >= 1 {
            iceStock++
            totalCash -= 1
            labelIce.text = "\(iceStock)"
            labelCash.text = "£\(totalCash)"
        }
    }
    
    @IBAction func unmixLemon(sender: UIButton) {
        
        if lemonMix > 0 {
            lemonMix--
            lemonStock++
            labelLemonMix.text = "\(lemonMix)"
            labelLemons.text = "\(lemonStock)"
            setSellButtonState()
        }
    }
    
    @IBAction func mixLemon(sender: UIButton) {
        
        if lemonStock > 0 {
            lemonMix++
            lemonStock--
            labelLemonMix.text = "\(lemonMix)"
            labelLemons.text = "\(lemonStock)"
            setSellButtonState()
        }
    }
    
    @IBAction func unmixIce(sender: UIButton) {

        if iceMix > 0 {
            iceMix--
            iceStock++
            labelIceMix.text = "\(iceMix)"
            labelIce.text = "\(iceStock)"
            setSellButtonState()
        }

    }
    
    @IBAction func mixIce(sender: UIButton) {

        if iceStock > 0 {
            iceMix++
            iceStock--
            labelIceMix.text = "\(iceMix)"
            labelIce.text = "\(iceStock)"
            setSellButtonState()
        }
    }
    
    @IBAction func sellLemonadeButtonPressed(sender: UIButton) {
        
        let mixRatio = Double(lemonMix) / Double(iceMix)

        var customerPotential = randomCustomerCount()
        var customerPref:[Double] = []
        var revenue = 0
        var customerCount = 0
        
        
        // Adjust number of customers depending on weather conditions
        customerPotential = adjustCustomersForWeather(weatherForecast, currentPotential: customerPotential)
        
        println("Acidity Ratio is \(lemonMix):\(iceMix) (\(mixRatio))")
        println("Potential Customers: \(customerPotential)")

        
        // Create array of customers' taste preferences

        for var i=0; i < customerPotential; i++ {
            
            customerPref.append(randomTastePreference())

        }

        
        // For each customer determine if the want to buy or not and calculate day's revenue
        for var i=0; i < customerPotential; i++ {
            if lemonadeDidSell (customerPref[i], ratio:mixRatio) {

                println ("Customer \(i+1) preference is \(customerPref[i]) *** SOLD! ***")
                revenue++
                customerCount++
                
                
            }
            else {
                
                println ("Customer \(i+1) preference is \(customerPref[i]) *** No Sale ***")
                
            }
        }
        
        
        // Update and reset variables ready for next day and also update
        // values on screen
        
        lemonMix = 0
        iceMix = 0
        totalCash += revenue
        dayCount++
        
        setSellButtonState()
        
        labelLemonMix.text = "0"
        labelIceMix.text = "0"
        
        labelCash.text = "£0"

        labelDay.text = "\(dayCount)"
        labelCustomers.text = "\(customerCount)\\\(customerPotential)"
        labelRevenue.text = "£\(revenue)"

        self.weatherForecast = generateRandomWeather()
        updateWeatherImage(self.weatherForecast)
        
        labelCash.text = "£\(totalCash)"
        
        
        // Game is over if cash reserves + value of stock holding < £3
        // as we are unable to buy one lemon and one ice cube
        
        if (totalCash + (lemonStock * 2) + iceStock) < 3 {
            showAlertWithText(header: "Game Over", message: "You don't have enough money to buy any more supplies.")
            resetGame()
        }
        
        
        
    }

    // Enable/Disable Sell Lemonade button depending on whether
    // we have enough lemons and ice (at least one of each)
    func setSellButtonState() {
        if lemonMix >= 1 && iceMix >= 1 {
            sellLemonadeButton.enabled = true
        }
        else {
            sellLemonadeButton.enabled = false
        }
    }
    
    // Return a random number of customers for the day (1...10)
    func randomCustomerCount(maxCustomers:Int = 10) -> Int {
        return Int(arc4random_uniform(UInt32(maxCustomers))+1)
    }
    
    
    // Return a random taste preference (0.00...1.00)
    func randomTastePreference() -> Double {
        return Double(arc4random_uniform(UInt32(101))) / 100.0
    }
    
    
    // Take a customer mix preference and the current mix to determine
    // if the customer wants to buy or not today.
    func lemonadeDidSell (customerPref:Double, ratio:Double) -> Bool {
        
        var sold = false
        
        if customerPref >= 0.0 && customerPref < 0.5 && ratio > 1.0 {
            // Acidic
            sold = true
        }
        else if customerPref >= 0.5 && customerPref < 0.7 && ratio == 1.0 {
            // Neutral
            sold = true
        }
        else if customerPref >= 0.7 && customerPref <= 1.0 && ratio < 1.0 {
            // Diluted
            sold = true
        }
        
        return sold
    }
    
    
    // Randomly generate one of three weather conditions (Cold/Mild/Warm)
    func generateRandomWeather() -> String {

        let weatherValue = Int(arc4random_uniform(UInt32(3)))
        
        switch weatherValue {
        case 0:
            return "Cold"
        case 1:
            return "Mild"
        case 2:
            return "Warm"
        default:
            return "!ERROR"
        }
    }
    
    // Update the weather forecast graphic
    func updateWeatherImage(imageName:String) {
        self.weatherImage.image = UIImage(named: imageName)
    }
    
    // Adjust the number of passining (potential) customers depending on weather
    func adjustCustomersForWeather (weatherForecast:String, currentPotential:Int) -> Int {

        var adjustedPotential = 0
        
        switch weatherForecast {

        case "Cold":
            adjustedPotential = currentPotential - 3
            if adjustedPotential < 0 {
                adjustedPotential = 0
            }

        case "Warm":
            adjustedPotential = currentPotential + 4
                
        default:
            adjustedPotential = currentPotential
        }
        println("--------------------")
        println("Day \(dayCount+1), Weather \(weatherForecast). Was \(currentPotential) customers, now \(adjustedPotential)")
        return adjustedPotential
    }

    
    // Reset the game to starting parameters
    func resetGame() {
        totalCash = 10
        lemonStock = 1
        iceStock = 1
        lemonMix = 0
        iceMix = 0
        dayCount = 0

        labelCash.text = "£\(totalCash)"
        
        labelLemons.text = "\(lemonStock)"
        labelIce.text = "\(iceStock)"
        
        labelLemonMix.text = "\(lemonMix)"
        labelIceMix.text = "\(iceMix)"
        
        labelDay.text = "0"
        labelCustomers.text = "0/0"
        labelRevenue.text = "£0"
        weatherForecast = generateRandomWeather()
        updateWeatherImage(weatherForecast)
        
        
    }

    // Generic function to display a popup message
    func showAlertWithText (header: String = "Warning!", message: String) {
        
        var alert = UIAlertController(title: header, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
    }


}

